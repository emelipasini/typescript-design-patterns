import { hello } from "../src/hello";

describe("hello", () => {
    it("should salute Clara", () => {
        expect(hello("Clara")).toBe("hello, Clara!");
    });
});
