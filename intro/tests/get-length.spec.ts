import { getLength } from "../src/get-length";

describe("get length", () => {
    it("should have length 3", () => {
        expect(getLength("abc")).toBe(3);
    });

    it("should have length 0", () => {
        expect(getLength("")).toBe(0);
    });
});
